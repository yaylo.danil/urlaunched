package com.youarelaunched.challenge

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.hasText
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import com.youarelaunched.challenge.data.repository.model.Vendor
import com.youarelaunched.challenge.middle.R
import com.youarelaunched.challenge.ui.screen.state.VendorsScreenUiState
import com.youarelaunched.challenge.ui.screen.view.VendorsScreen
import org.junit.Rule
import org.junit.Test

class VendorsScreenTest {

    @get:Rule
    val rule = createAndroidComposeRule<MainActivity>()

    private val stateWithEmptyList = VendorsScreenUiState(null)
    private val stateWithList =
        VendorsScreenUiState(listOf(Vendor(1, "testCompanyName", "", "", true, listOf(), "")))

    @Test
    fun vendorsListEmpty_noResultsItemVisible() {
        rule.setContent { VendorsScreen(uiState = stateWithEmptyList) }
        rule.onNode(hasText(rule.activity.getString(R.string.empty_item_title)))
            .assertIsDisplayed()
    }

    @Test
    fun vendorsListNotEmpty_ListDisplayed() {
        rule.setContent { VendorsScreen(uiState = stateWithList) }
        rule.onNode(hasText("testCompanyName"))
            .assertIsDisplayed()
    }
}
