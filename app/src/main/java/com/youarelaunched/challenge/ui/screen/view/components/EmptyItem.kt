package com.youarelaunched.challenge.ui.screen.view.components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.sp
import com.youarelaunched.challenge.middle.R
import com.youarelaunched.challenge.ui.theme.VendorAppTheme

@Composable
fun EmptyItem() {
    Column {
        Text(
            text = stringResource(id = R.string.empty_item_title),
            textAlign = TextAlign.Center,
            fontSize = 24.sp,
            fontWeight = FontWeight.Bold,
            color = VendorAppTheme.colors.textTitle,
            modifier = Modifier.fillMaxWidth()
        )
        Text(
            text = stringResource(id = R.string.empty_item_text),
            textAlign = TextAlign.Center,
            fontSize = 16.sp,
            color = VendorAppTheme.colors.textDark,
            modifier = Modifier.fillMaxWidth()
        )
    }
}
