package com.youarelaunched.challenge.ui.screen.view.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Search
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.youarelaunched.challenge.ui.screen.view.VendorsVM
import com.youarelaunched.challenge.ui.theme.VendorAppTheme
import com.youarelaunched.challenge.middle.R
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

private val MIN_INPUT_SYMBOLS = 3
private val DEBOUNCING = 500L

@Composable
fun SearchField(
    viewModel: VendorsVM
) {
    val text = remember { mutableStateOf(TextFieldValue("")) }
    val coroutineScope = rememberCoroutineScope()

    TextField(
        value = text.value,
        onValueChange = { value ->
            text.value = value
            if (value.text.length >= MIN_INPUT_SYMBOLS) {
                coroutineScope.launch {
                    delay(DEBOUNCING)
                    viewModel.getVendors(value.text)
                }
            }
        },
        placeholder = { Text(text = stringResource(id = R.string.search_hint)) },
        modifier = Modifier
            .padding(10.dp)
            .fillMaxWidth()
            .shadow(elevation = 10.dp, shape = RoundedCornerShape(16.dp)),
        textStyle = TextStyle(color = VendorAppTheme.colors.textDark, fontSize = 18.sp),
        trailingIcon = {
            Icon(
                painter = painterResource(id = R.drawable.icon_search),
                contentDescription = "",
                modifier = Modifier
                    .padding(15.dp)
                    .size(24.dp)
                    .clickable {
                        viewModel.getVendors(text.value.text)
                    }
            )
        },
        singleLine = true,
        shape = RoundedCornerShape(16.dp),
        colors = TextFieldDefaults.textFieldColors(
            textColor = VendorAppTheme.colors.textDark,
            placeholderColor = VendorAppTheme.colors.text,
            cursorColor = VendorAppTheme.colors.text,
            errorCursorColor = VendorAppTheme.colors.text,
            leadingIconColor = Color.White,
            trailingIconColor = VendorAppTheme.colors.text,
            backgroundColor = Color.White,
            focusedIndicatorColor = Color.Transparent,
            unfocusedIndicatorColor = Color.Transparent,
            disabledIndicatorColor = Color.Transparent
        )
    )

}
