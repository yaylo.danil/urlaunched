package com.youarelaunched.challenge

import com.google.common.truth.Truth.assertThat
import com.youarelaunched.challenge.repositories.VendorsRepositoryFake
import com.youarelaunched.challenge.ui.screen.view.VendorsVM
import kotlinx.coroutines.*
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.TestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestWatcher
import org.junit.runner.Description

class VendorsVMTest {
    private lateinit var viewModel: VendorsVM

    @ExperimentalCoroutinesApi
    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()


    @ExperimentalCoroutinesApi
    @Before
    fun setup(){
        viewModel = VendorsVM(VendorsRepositoryFake())
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `data was loaded successfully and the list contains at least one item`() {
        viewModel.getVendors("")
        CoroutineScope(mainDispatcherRule.dispatcher).launch {
            delay(1)
            assertThat(viewModel.uiState.value.vendors.isNullOrEmpty()).isFalse()
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `data loaded with error`() {
        viewModel.getVendors("QueryToEmptyList")
        CoroutineScope(mainDispatcherRule.dispatcher).launch {
            delay(1)
            assertThat(viewModel.uiState.value.vendors.isNullOrEmpty()).isTrue()
        }
    }
}

@ExperimentalCoroutinesApi
class MainDispatcherRule(val dispatcher: TestDispatcher = StandardTestDispatcher()): TestWatcher() {

    override fun starting(description: Description) = Dispatchers.setMain(dispatcher)

    override fun finished(description: Description) = Dispatchers.resetMain()

}
