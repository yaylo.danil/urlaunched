package com.youarelaunched.challenge.repositories

import com.youarelaunched.challenge.data.repository.VendorsRepository
import com.youarelaunched.challenge.data.repository.model.Vendor

class VendorsRepositoryFake : VendorsRepository {
    private val vendorsList = listOf(Vendor(1, "", "", "", true, listOf(), ""))

    override suspend fun getVendors(query: String): List<Vendor> {
        return vendorsList
    }
}
